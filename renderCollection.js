Knack=Knack||{}; Knack.fn=Knack.fn||{}; Knack.data=Knack.data||{};

Knack.fn.renderCollection = function(sceneId){

	var scene_id = sceneId;
	
	let defaults = {
		onBeforeSend : null,
		onAfterSend : null,
		formatResponse : null,
		find : null,
		onDone : null,
		onError : null,
		onComplete : null,
	};

	var headers = {
		"X-Knack-Application-Id": Knack.application_id,
		"X-Knack-REST-API-Key": "renderer",
		Authorization: Knack.getUserToken()
	};

	let _setting = {};

	let methods = {};

	let _filter = null;
	let _dfSettings = {
		filters : false,
	};
	let data = {};
	let setts = Object.assign({}, defaults);

	var responsefn = function(dt, opt){
		typeof opt.iterateRecord === 'function' ? opt.iterateRecord(dt) : '';
		typeof opt.onComplete === 'function' ? opt.onComplete(dt) : '';
		typeof opt.onDone === 'function' ? opt.onDone(dt) : '';

		let data_obj = dt;
		return {
		  data : data_obj,
		  opt  : opt
		}
	}

	methods.view = function(viewId){
		let view_id = viewId;
		var setts = setts||{}, _dfSettings = _dfSettings||{};
		var fn = {};
		fn.get = function(callback, filters, page = 1, id = ''){
			let filters_ = typeof filters==="object"?`?rows_per_page=1000&page=${page}&filters=`+encodeURIComponent(JSON.stringify(filters)):`?rows_per_page=1000&page=${page}`;
			Knack.$.ajax({
				url : `https://us-east-1-renderer-read.knack.com/v1/scenes/${scene_id}/views/${view_id}/records${filters_}`,
				type : 'GET', 
				headers : headers, success : function(data){ typeof callback === 'function' ? callback(data) : false; }
			});
		};

		return {
			collection : function(options, filters, start_page = 1, end_page = false ){

				ajax_collection = function(page_index, opt, callback, data_return = []){

	              fn.get(function(response){

	                data_return = data_return.concat(response.records);

	                if(typeof response == 'object' && response.total_pages){
	                  if(page_index < response.total_pages && (!end_page || (page_index < end_page)) ){
	                    ajax_collection(++page_index, opt, callback, data_return);
	                  }else{
	                    typeof callback === 'function' && callback(data_return, 200) ; responsefn(data_return, opt);
	                  }
	                }else{
	                  console.error('response is not object');
	                  typeof callback === 'function' && callback(data_return, 201) ; responsefn(data_return, opt);
	                }

	              }, opt.filters, page_index);

	          };


	          opt = Object.assign({}, setts, _dfSettings, options);

	          ajax_collection(start_page, opt, (typeof options === 'function' ? options : false) );
			}, 
			page : function(){

			},
			record : function(id ){

			}
		}
	}

	return methods;


}
